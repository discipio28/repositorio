package Santamaria.Angie;

public class Camiseta {

    private int itemID, codigo;
    private String descripcion, medida;
    private double precio; //hides code but with get/set you can access these

    //parameterized constructor
    public Camiseta(int pItemID, int pCodigo, String pMedida, String pDescripcion, double pPrecio) {
        this.itemID = pItemID; // parameters are different than the properties
        this.codigo = pCodigo;
        this.medida = pMedida;
        this.descripcion = pDescripcion;
        this.precio = pPrecio;
    }

    @Override
    public String toString() {
        return "Camisetas registradas: " +
                "\nID: " + itemID +
                "\nCódigo de color: " + codigo +
                "\nDescripción: " + descripcion +
                "\nMedida: " + medida +
                "\nPrecio: " + precio + "";
    }

    public void setItemID(int itemID){ // sets item ID
        this.itemID = itemID;
    }

    public void setCodigo(int codigo){ // sets color code
        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion){ // sets description
        this.descripcion = descripcion;
    }

    public void setMedida(String medida){ // sets measurements
        this.medida = medida;
    }

    public void setPrecio(double precio){ // sets price
        this.precio = precio;
    }
}
