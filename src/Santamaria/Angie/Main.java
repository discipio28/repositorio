package Santamaria.Angie;

import java.io.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    static void menu() {
        out.println("1. Crear camiseta");
        out.println("2. Listar camisetas");
        out.println("3. Registrar cliente");
        out.println("4. Listar clientes");
        out.println("5. Salir");
    }

    static void tamanoArregloCamiseta() throws IOException{
        out.print("Ingrese la cantidad de camisetas a crear: ");
        int cantidad = Integer.parseInt(in.readLine());
        BL.inicializarArregloCamiseta(cantidad);
    }

    static void tamanoArregloCliente() throws IOException{
        out.print("Ingrese la cantidad de clientes a registrar: ");
        int cantidad = Integer.parseInt(in.readLine());
        BL.inicializarArregloCliente(cantidad);
    }

    static void crearCamiseta() throws IOException{ //registra la camiseta
        out.print("Ingrese el ID de la camiseta: ");
        int itemID = Integer.parseInt(in.readLine());

        out.print("Ingrese el código de color de la camiseta: ");
        int codigo = Integer.parseInt(in.readLine());

        out.print("Ingrese la medida de la camiseta (L, M, S): ");
        String medida = in.readLine(); //cambiar a CHAR

        out.print("Ingrese el precio de la camiseta: ");
        double precio = Double.parseDouble(in.readLine());

        out.print("Ingrese una descripción para la camiseta: ");
        String descripcion = in.readLine();

        String mensaje = BL.crearCamiseta(itemID, codigo, medida, descripcion, precio);
        out.println(mensaje);
    }

    static void registrarCliente() throws IOException{ //registra al cliente
        out.print("Ingrese el nombre del cliente: ");
        String nombre = in.readLine();

        out.print("Ingrese el primer apellido del cliente: ");
        String primerApellido = in.readLine();

        out.print("Ingrese el segundo apellido del cliente: ");
        String segundoApellido = in.readLine();

        out.print("Ingrese la dirección del cliente: : ");
        String direccion = in.readLine();

        out.print("Ingrese un correo del cliente: ");
        String correo = in.readLine();

        String mensaje = BL.registrarCliente(nombre, primerApellido, segundoApellido, direccion, correo);
        out.println(mensaje);
    }

    static void listarCamisetas(){
        String[] listaCamisetas = BL.listarCamisetas();

        for(int i = 0;i< listaCamisetas.length;i++){
            out.println(listaCamisetas[i]);
        }
    }

    static void listarClientes(){
        String[] listaClientes = BL.listarClientes();

        for(int i = 0;i< listaClientes.length;i++){
            out.println(listaClientes[i]);
        }
    }

    static int ingresarOpcion() throws IOException{
        out.print("Ingrese la opción que desea realizar: ");
        return Integer.parseInt(in.readLine());
    }

    static void seleccionarOpcion(int menuOpcion) throws IOException {
        switch (menuOpcion) {
            case 1:
                tamanoArregloCamiseta(); // this must loop until the size of the array is completed
                crearCamiseta();
                break;

            case 2:
                listarCamisetas();
                break;

            case 3:
                tamanoArregloCliente();
                registrarCliente();
                break;

            case 4:
                listarClientes();
                break;

            case 5:
                out.println("Saliendo del programa...");
                break;
        }
    }
    public static void main(String[] args) throws IOException{
        int opcion;
        do {
            menu();
            opcion = ingresarOpcion();
            seleccionarOpcion(opcion);
        } while (opcion != 0);
    }
}
