package Santamaria.Angie;

public class Cliente {

    private String nombre, primerApellido, segundoApellido, direccion, correo;

    //parameterized constructor
    public Cliente (String pNombre, String pPrimerApellido, String pSegundoApellido, String pDireccion, String pCorreo) {
        this.nombre = pNombre; // parameters are different than the properties
        this.primerApellido = pPrimerApellido;
        this.segundoApellido = pSegundoApellido;
        this.direccion = pDireccion;
        this.correo = pCorreo;
    }

    @Override
    public String toString() {
        return "Clientes registrados: " +
                "\nNombre: " + nombre +
                "\nPrimer Apellido: " + primerApellido +
                "\nSegundo Apellido: " + segundoApellido +
                "\nDirección: " + direccion +
                "\nCorreo: " + correo + "";
    }

    public void setNombre(String nombre){ // sets name
        this.nombre = nombre;
    }

    public void setPrimerApellido(String primerApellido){ // sets first last name
        this.primerApellido = primerApellido;
    }

    public void setSegundoApellido(String segundoApellido){ // sets second last name
        this.segundoApellido = segundoApellido;
    }

    public void setDireccion(String direccion){ // sets address
        this.direccion = direccion;
    }

    public void setCorreo(String correo){ // sets email
        this.correo = correo;
    }
}