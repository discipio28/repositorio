package Santamaria.Angie;

public class BL {

    static Camiseta[] camisetas;
    static int camisetasGuardadas;

    static Cliente[] clientes;
    static int clientesGuardados;

    static void inicializarArregloCliente(int tamanoArreglo) {
        clientes = new Cliente[tamanoArreglo];
        clientesGuardados = 0;
    }

    static void inicializarArregloCamiseta(int tamanoArreglo) {
        camisetas = new Camiseta[tamanoArreglo];
        camisetasGuardadas = 0;
    }

    static String crearCamiseta(int itemID, int codigo, String medida, String descripcion, double precio) {

        Camiseta nuevaCamiseta = new Camiseta(itemID, codigo, medida, descripcion, precio);

        nuevaCamiseta.setItemID(itemID);
        nuevaCamiseta.setCodigo(codigo);
        nuevaCamiseta.setMedida(medida);
        nuevaCamiseta.setDescripcion(descripcion);
        nuevaCamiseta.setPrecio(precio);

        camisetas[camisetasGuardadas] = new Camiseta(itemID, codigo, medida, descripcion, precio);

        camisetasGuardadas++;

        return "Camiseta creada";
    }

    static String registrarCliente(String nombre, String primerApellido, String segundoApellido, String direccion, String correo) {

        Cliente nuevoCliente = new Cliente(nombre, primerApellido, segundoApellido, direccion, correo);

        nuevoCliente.setNombre(nombre);
        nuevoCliente.setPrimerApellido(primerApellido);
        nuevoCliente.setSegundoApellido(segundoApellido);
        nuevoCliente.setDireccion(direccion);
        nuevoCliente.setCorreo(correo);

        clientes[clientesGuardados] = new Cliente(nombre, primerApellido, segundoApellido, direccion, correo);

        clientesGuardados++;

        return "Cliente registrado";
    }

    static String[] listarCamisetas(){
        String [] guardarCamiseta = new String[camisetas.length];
        for(int i=0; i<camisetas.length; i++){
            guardarCamiseta[i] = String.valueOf(camisetas[i]);
        }
        return guardarCamiseta;
    }

    static String[] listarClientes(){
        String [] guardarCliente = new String[clientes.length];
        for(int i=0; i<clientes.length; i++){
            guardarCliente[i] = String.valueOf(clientes[i]);
        }
        return guardarCliente;
    }
}
